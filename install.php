<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Couplet Installation</title>

    <!-- Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/skeleton/2.0.4/skeleton.css" rel="stylesheet">


  </head>
  <body>
<div class="container">

  <!-- columns should be the immediate child of a .row -->
  <div class="row">
    <div class="one column"></div>
    <div class="ten columns">
	
<br />
<br />
<br />
<h5>Server requirements</h5>
<?php

extension_check(array( 
	'openssl',
	'pdo', 
	'pdo_mysql', 
	'mbstring', 
	'tokenizer'
));
function extension_check($extensions) {
	$fail = '';
	$pass = '';
	
	if(version_compare(PHP_VERSION, '5.6.4', '<')) {
		$fail .= '<li>You need<strong> PHP 5.6.4</strong> (or greater). You have PHP '.PHP_VERSION.'</li>';
	}
	else {
		$pass .='<li>You have<strong> PHP '.PHP_VERSION.'</strong></li>';
	}
	foreach($extensions as $extension) {
		if(!extension_loaded($extension)) {
			$fail .= '<li> You are missing the <strong>'.$extension.'</strong> extension</li>';
		}
		else{	$pass .= '<li>You have the <strong>'.$extension.'</strong> extension</li>';
		}
	}
	
	if($fail) {
		echo '<p><strong>Your server does not meet the following requirements in order to install Couplet.</strong>';
		echo '<br>The following requirements failed, please contact your hosting provider in order to receive assistance with meeting the system requirements for Couplet:';
		echo '<ul>'.$fail.'</ul></p>';
		echo 'The following requirements were successfully met:';
		echo '<ul>'.$pass.'</ul>';
		die();
	} else {
		echo '<p><strong>Congratulations!</strong> Your server meets the requirements for Couplet.</p>';
		echo '<ul>'.$pass.'</ul>';
	}
}
?>

<h5>Unzipping</h5>
<?

$zip = new ZipArchive;
if ($zip->open('full.zip') === TRUE) {
    $zip->extractTo(__DIR__);
    $zip->close();
	@unlink('full.zip');
    echo 'Successfully unzipped.';
	
	@symlink(__DIR__.'/storage/app/public', __DIR__.'/public/storage');
	@symlink(__DIR__.'/admin', __DIR__.'/public/admin');
	
} else {
    echo 'Unable to unzip. Please make sure you have the correct permissions. Please contact us on contact@getcouplet.com.';
	die();
}

?>
	<small>
		<p>* Remember have mod_rewrite enabled and give permissions to folders (storage and bootstrap/cache)<br>
		chmod -R 777 bootstrap/cache; chmod -R 777 storage</p>
	</small>

	<a href="./install" class="button  button-primary">Click here to set up your database</a>

	
	</div>
	<div class="one column"></div>
  </div>
  </body>
</html>